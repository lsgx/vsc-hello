// hello.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h" // stdio.h, tchar.h, targetver.h

#if defined(UNICODE) && defined(_UNICODE)
#pragma message("UNICODE and _UNICODE macro activated.")
#else
#error UNICODE and _UNICODE must be defined. This version only supports unicode builds.
#endif // !UNICONDE

#include <cstdio> // fprintf, fwprintf
#include <cstdlib> // system, _wsystem
#include <clocale> // setlocale, _wsetlocale

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif /* End of defined(__cplusplus) || defined(c_plusplus) */

int sayHello()
{
    int iret = 0;
    _ftprintf(stdout, _T("Say: %s\n"), _T("Hello 您好"));
    return iret;
}

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif /* End of defined(__cplusplus) || defined(c_plusplus) */

int main(int argc, char *argv[], char *envp[])
{
    int iret = 0;
    TCHAR *oldLocale = nullptr;
    const TCHAR *LOCALE_STR = _T("chs");
    oldLocale = _tsetlocale(LC_ALL, nullptr);
    _ftprintf(stdout, _T("%s: %s %s.\n"), _T("notice"), _T("initial locale is"), oldLocale);

    /*
    * string --- locale
    * NULL --- C
    * "" --- Chinese (Simplified)_China.936
    * "zh-CN" --- zh-CN
    * "chs" --- Chinese_China.936
    */
    if (!_tsetlocale(LC_ALL, LOCALE_STR))
    {
        _ftprintf(stderr, _T("%s: %s %s %s.\n"), _T("error"), _T("setlocale"), LOCALE_STR, _T("failure"));
        iret = -1;
        return iret;
    }
    _ftprintf(stdout, _T("%s: %s %s.\n"), _T("notice"), _T("current locale is"), _tsetlocale(LC_ALL, nullptr));

    sayHello();

    _tsystem(_T("Pause"));
    iret = 0;
    return iret;
}
