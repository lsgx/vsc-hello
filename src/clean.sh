#!/usr/bin/env bash
#filename: clean.sh

read -p "Start clean project --- $1"
/usr/bin/env gmake clean
/usr/bin/env gmake clean_objects
/usr/bin/env gmake clean_depend
/usr/bin/env gmake clean_profile
read -p "End clean project --- $1"
